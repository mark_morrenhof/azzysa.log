﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Log
{
    public class LogHandler
    {
        public static void Log(string json)
        {
            var log = JsonConvert.DeserializeObject<LogEntry>(json);

            switch (log.LogType)
            {
                case "document":
                    var log_document = JsonConvert.DeserializeObject<DocumentLogEntry>(json);
                    log_document.Insert();
                    break;
                case "log":
                default:
                    log.Insert();
                    break;
            }
        }

        public static void RegisterDatabaseConnectionString(string connectionString)
        {
            DatabaseHelper.ConnectionString = connectionString;
            DatabaseHelper.Verify();
        }

        public static void Log(LogEntry logEntry, string apiUrl)
        {
            logEntry.Push(apiUrl);
        }
    }
}