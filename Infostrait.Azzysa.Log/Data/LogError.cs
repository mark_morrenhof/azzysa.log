﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Azzysa.Log
{
    public class LogError
    {
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
        [JsonProperty("stacktrace")]
        public string Stacktrace { get; set; }
    }
}
