﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Azzysa.Log
{
    internal class DatabaseHelper
    {
        internal static string ConnectionString { get; set; }

        #region Verify
        internal static void Verify()
        {
            using (var sqlConn = new SqlConnection(ConnectionString))
            {
                sqlConn.Open();

                #region Verify Tables
                var tables = GetTables(sqlConn);

                if (!tables.Contains("Log"))
                {
                    string query = @"CREATE TABLE [dbo].[Log](
	                                        [Id] [uniqueidentifier] NOT NULL,
                                            [BatchId] [uniqueidentifier] NULL,
                                            [ClassName] [varchar](50) NULL,
                                            [Handler] [varchar](50) NULL,
	                                        [Date] [datetime] NULL,
	                                        [OriginId] [varchar](50) NULL,
	                                        [ObjectId] [varchar](50) NULL,
                                            [BusinessObjectId] [varchar](50) NULL,
	                                        [Status] [varchar](50) NULL,
	                                        [Duration] [int] NULL,
                                            [ObjectExists] [bit] NULL,
                                            [ParentExists] [bit] NULL,
                                            [Type] [varchar](250) NULL,
                                            [Name] [varchar](250) NULL,
                                            [Revision] [varchar](250) NULL,
                                            [ParentRevision] [varchar](250) NULL,
                                            [Policy] [varchar](250) NULL,
                                            [ErrorMessage] [text] NULL,
                                            [Stacktrace] [text] NULL,
                                            [Reason] [varchar](250) NULL,
                                            [Details] [text] NULL,
	                                        CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
	                                        (
		                                        [Id] ASC
	                                        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

                    ExecuteNonQuery(query, sqlConn);
                }

                if (!tables.Contains("DocumentLog"))
                {
                    string query = @"CREATE TABLE [dbo].[DocumentLog](
	                                        [Id] [uniqueidentifier] NOT NULL,
                                            [FileName] [varchar](250) NULL,
                                            [Format] [varchar](50) NULL,
                                            [FileType] [varchar](50) NULL,
	                                        [Title] [varchar](250) NULL,
                                            [Path] [varchar](250) NULL,
                                            [FileExists] [bit] NULL,
                                            [VersionId] [varchar](50) NULL,
                                            [ParentVersionExists] [bit] NULL,
                                            [Size] [bigint] NULL,
                                            [RelatedObject] [varchar](250) NULL,
                                            [State] [varchar](50) NULL
	                                        CONSTRAINT [PK_DocumentLog] PRIMARY KEY CLUSTERED 
	                                        (
		                                        [Id] ASC
	                                        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                        ) ON [PRIMARY]";

                    ExecuteNonQuery(query, sqlConn);
                }

                if (!tables.Contains("LogError"))
                {
                    string query = @"CREATE TABLE [dbo].[LogError](
	                                            [Id] [int] IDENTITY(1,1) NOT NULL,
	                                            [LogId] [uniqueidentifier] NOT NULL,
	                                            [ErrorMessage] [text] NULL,
                                                [Stacktrace] [text] NULL,
                                             CONSTRAINT [PK_LogError] PRIMARY KEY CLUSTERED 
                                            (
	                                            [Id] ASC
                                            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

                    ExecuteNonQuery(query, sqlConn);
                }

                if (!tables.Contains("LogMessage"))
                {
                    string query = @"CREATE TABLE [dbo].[LogMessage](
	                                            [Id] [int] IDENTITY(1,1) NOT NULL,
	                                            [LogId] [uniqueidentifier] NOT NULL,
	                                            [Message] [text] NULL,
                                             CONSTRAINT [PK_LogMessage] PRIMARY KEY CLUSTERED 
                                            (
	                                            [Id] ASC
                                            )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                            ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

                    ExecuteNonQuery(query, sqlConn);
                }
                #endregion

                #region Verify Views

                if (!tables.Contains("LogOverview"))
                {
                    string query = @"create view [dbo].[LogOverview]
                                     as
                                     select
                                        [ClassName],
                                        count(distinct case when [Status] = 'Succeeded' then [Log].Id else null end) as [Succeeded],
                                        count(distinct case when [Status] = 'CompletedWithErrors' then [Log].Id else null end) as [CompletedWithErrors],
                                        count(distinct case when [Status] = 'Skipped' then [Log].Id else null end) as [Skipped],
                                        count(distinct case when [Status] = 'Invalid' then [Log].Id else null end) as [Invalid],
                                        count(distinct case when [Status] = 'Failed' then [Log].Id else null end) as [Failed],
                                        count(distinct[Log].Id) as [Total]
                                    from [Log]
                                    group by [ClassName]";

                    ExecuteNonQuery(query, sqlConn);
                }

        #endregion
    }
        }
        #endregion

        #region Insert

        #region Insert Log Messages
        public static void InsertLogMessages(LogEntry entry)
        {
            //Delete old messages
            ExecuteNonQuery("delete from [LogMessage] where [LogId] = @LogId", new List<SqlParameter>() { new SqlParameter("LogId", entry.Id) });

            foreach (var message in entry.Messages)
            {
                var parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("LogId", entry.Id));
                parameters.Add(new SqlParameter("Message", message == null ? (object)DBNull.Value : message));

                ExecuteNonQuery("insert into [LogMessage] ([LogId], [Message]) values (@LogId, @Message)", parameters);
            }
        }
        #endregion

        #region Insert Log Errors
        public static void InsertLogErrors(LogEntry entry)
        {
            //Delete old messages
            ExecuteNonQuery("delete from [LogError] where [LogId] = @LogId", new List<SqlParameter>() { new SqlParameter("LogId", entry.Id) });

            foreach (var error in entry.Errors)
            {
                var parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("LogId", entry.Id));
                parameters.Add(new SqlParameter("ErrorMessage", error.ErrorMessage == null ? (object)DBNull.Value : error.ErrorMessage));
                parameters.Add(new SqlParameter("Stacktrace", error.Stacktrace == null ? (object)DBNull.Value : error.Stacktrace));

                ExecuteNonQuery("insert into [LogError] ([LogId], [ErrorMessage], [Stacktrace]) values (@LogId, @ErrorMessage, @Stacktrace)", parameters);
            }
        }
        #endregion

        #region Insert Log Entry
        public static void InsertLogEntry(LogEntry entry)
        {
            var query = @"delete from [Log] where [Id] = @Id
                          insert into [Log] ([Id], [BatchId], [ClassName], [Handler], [Date], [OriginId], [ObjectId], [BusinessObjectId], [Status], [Duration], [ObjectExists], [ParentExists], [Type], [Name], [Revision], [ParentRevision], [Policy], [ErrorMessage], [Stacktrace], [Reason], [Details])
                          values (@Id, @BatchId, @ClassName, @Handler, @Date, @OriginId, @ObjectId, @BusinessObjectId, @Status, @Duration, @ObjectExists, @ParentExists, @Type, @Name, @Revision, @ParentRevision, @Policy, @ErrorMessage, @Stacktrace, @Reason, @Details)";

            #region Parameters
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("Id", entry.Id));
            parameters.Add(new SqlParameter("BatchId", !entry.BatchId.HasValue ? (object)DBNull.Value : entry.BatchId.Value));
            parameters.Add(new SqlParameter("ClassName", entry.ClassName == null ? (object)DBNull.Value : entry.ClassName));
            parameters.Add(new SqlParameter("Handler", entry.Handler == null ? (object)DBNull.Value : entry.Handler));
            parameters.Add(new SqlParameter("Date", !entry.Date.HasValue ? (object)DBNull.Value : new DateTime(1970, 1, 1, 1, 0, 0).AddMilliseconds(entry.Date.Value)));
            parameters.Add(new SqlParameter("OriginId", entry.OriginId == null ? (object)DBNull.Value : entry.OriginId));
            parameters.Add(new SqlParameter("ObjectId", entry.ObjectId == null ? (object)DBNull.Value : entry.ObjectId));
            parameters.Add(new SqlParameter("BusinessObjectId", entry.BusinessObjectId == null ? (object)DBNull.Value : entry.BusinessObjectId));
            parameters.Add(new SqlParameter("Status", entry.Status == null ? (object)DBNull.Value : entry.Status));
            parameters.Add(new SqlParameter("Duration", !entry.Duration.HasValue ? (object)DBNull.Value : entry.Duration.Value));
            parameters.Add(new SqlParameter("ObjectExists", !entry.ObjectExists.HasValue ? (object)DBNull.Value : entry.ObjectExists.Value));
            parameters.Add(new SqlParameter("ParentExists", !entry.ParentExists.HasValue ? (object)DBNull.Value : entry.ParentExists.Value));
            parameters.Add(new SqlParameter("Type", entry.Type == null ? (object)DBNull.Value : entry.Type));
            parameters.Add(new SqlParameter("Name", entry.Name == null ? (object)DBNull.Value : entry.Name));
            parameters.Add(new SqlParameter("Revision", entry.Revision == null ? (object)DBNull.Value : entry.Revision));
            parameters.Add(new SqlParameter("ParentRevision", entry.ParentRevision == null ? (object)DBNull.Value : entry.ParentRevision));
            parameters.Add(new SqlParameter("Policy", entry.Policy == null ? (object)DBNull.Value : entry.Policy));
            parameters.Add(new SqlParameter("ErrorMessage", entry.ErrorMessage == null ? (object)DBNull.Value : entry.ErrorMessage));
            parameters.Add(new SqlParameter("Stacktrace", entry.Stacktrace == null ? (object)DBNull.Value : entry.Stacktrace));
            parameters.Add(new SqlParameter("Reason", entry.Reason == null ? (object)DBNull.Value : entry.Reason));
            parameters.Add(new SqlParameter("Details", entry.Details == null ? (object)DBNull.Value : entry.Details));
            #endregion

            ExecuteNonQuery(query, parameters);
        }
        #endregion

        #region Insert Document Log Entry
        public static void InsertDocumentLogEntry(DocumentLogEntry entry)
        {
            var query = @"delete from [DocumentLog] where [Id] = @Id
                          insert into [DocumentLog] ([Id], [FileName], [Format], [FileType], [Title], [Path], [FileExists], [VersionId], [ParentVersionExists], [Size], [RelatedObject], [State]) values (@Id, @FileName, @Format, @FileType, @Title, @Path, @FileExists, @VersionId, @ParentVersionExists, @Size, @RelatedObject, @State)";

            #region Parameters
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("Id", entry.Id));
            parameters.Add(new SqlParameter("FileName", entry.FileName == null ? (object)DBNull.Value : entry.FileName));
            parameters.Add(new SqlParameter("Format", entry.Format == null ? (object)DBNull.Value : entry.Format));
            parameters.Add(new SqlParameter("FileType", entry.FileType == null ? (object)DBNull.Value : entry.FileType));
            parameters.Add(new SqlParameter("Title", entry.Title == null ? (object)DBNull.Value : entry.Title));
            parameters.Add(new SqlParameter("Path", entry.Path == null ? (object)DBNull.Value : entry.Path));
            parameters.Add(new SqlParameter("FileExists", !entry.FileExists.HasValue ? (object)DBNull.Value : entry.FileExists.Value));
            parameters.Add(new SqlParameter("VersionId", entry.VersionId == null ? (object)DBNull.Value : entry.VersionId));
            parameters.Add(new SqlParameter("ParentVersionExists", !entry.ParentVersionExists.HasValue ? (object)DBNull.Value : entry.ParentVersionExists.Value));
            parameters.Add(new SqlParameter("Size", !entry.Size.HasValue ? (object)DBNull.Value : entry.Size.Value));
            parameters.Add(new SqlParameter("RelatedObject", entry.RelatedObject == null ? (object)DBNull.Value : entry.RelatedObject));
            parameters.Add(new SqlParameter("State", entry.State == null ? (object)DBNull.Value : entry.State));
            #endregion

            ExecuteNonQuery(query, parameters);
        }
        #endregion

        #endregion

        #region Helper Functions

        #region ExecuteNonQuery
        public static void ExecuteNonQuery(string query, DbConnection connection, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (connection == null)
            {
                ExecuteNonQuery(query, parameters, transaction);
                return;
            }

            using (var c = connection.CreateCommand())
            {
                c.CommandText = query;

                if (transaction != null)
                    c.Transaction = transaction;

                if (parameters != null)
                {
                    foreach (var parameter in parameters)
                        c.Parameters.Add(parameter);
                }

                c.ExecuteNonQuery();
            }
        }

        public static void ExecuteNonQuery(string query, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (transaction != null)
                ExecuteNonQuery(query, transaction.Connection, parameters, transaction);
            else {
                using (var connection = new SqlConnection(ConnectionString))
                {
                    connection.Open();
                    ExecuteNonQuery(query, connection, parameters, transaction);
                }
            }
        }

        public static T ExecuteScalar<T>(string query, DbConnection connection, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (connection == null)
                return ExecuteScalar<T>(query, parameters, transaction);

            using (var c = connection.CreateCommand())
            {
                c.CommandText = query;

                if (transaction != null)
                    c.Transaction = transaction;

                if (parameters != null)
                {
                    foreach (var parameter in parameters)
                        c.Parameters.Add(parameter);
                }

                var obj = c.ExecuteScalar();

                if (obj == DBNull.Value)
                    return default(T);
                return (T)Convert.ChangeType(obj, typeof(T));
            }
        }

        public static T ExecuteScalar<T>(string query, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (transaction != null)
                return ExecuteScalar<T>(query, transaction.Connection, parameters, transaction);

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                return ExecuteScalar<T>(query, connection, parameters, transaction);
            }
        }
        #endregion

        #region Get Tables
        private static List<string> GetTables(SqlConnection sqlConn)
        {
            var tables = new List<string>();

            using (var cmd = new SqlCommand("select * from information_schema.tables with (nolock)", sqlConn))
            {
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        tables.Add((string)dr["TABLE_NAME"]);
                }
            }

            return tables;
        }
        #endregion

        #endregion
    }
}
