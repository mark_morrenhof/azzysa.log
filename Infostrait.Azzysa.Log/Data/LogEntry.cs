﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Log
{
    public class LogEntry : ILogEntry
    {
        [JsonProperty("logType")]
        public string LogType { get; set; }
        [JsonProperty("id")]
        public Guid Id { get; set; } = Guid.NewGuid();
        [JsonProperty("batchId")]
        public Guid? BatchId { get; set; } = Guid.Empty;
        [JsonProperty("className")]
        public string ClassName { get; set; }
        [JsonProperty("handler")]
        public string Handler { get; set; }
        [JsonProperty("date")]
        public long? Date { get; set; } = (long)Math.Floor(DateTime.Now.Subtract(new DateTime(1970, 1, 1, 1, 0, 0)).TotalMilliseconds);
        [JsonProperty("legacyOriginId")]
        public string OriginId { get; set; }
        [JsonProperty("legacyObjectId")]
        public string ObjectId { get; set; }
        [JsonProperty("objectId")]
        public string BusinessObjectId { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("duration")]
        public long? Duration { get; set; }
        [JsonProperty("objectExists")]
        public bool? ObjectExists { get; set; }
        [JsonProperty("parentExists")]
        public bool? ParentExists { get; set; }
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("revision")]
        public string Revision { get; set; }
        [JsonProperty("parentRevision")]
        public string ParentRevision { get; set; }
        [JsonProperty("policy")]
        public string Policy { get; set; }
        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }
        [JsonProperty("stacktrace")]
        public string Stacktrace { get; set; }
        [JsonProperty("reason")]
        public string Reason { get; set; }
        [JsonProperty("details")]
        public string Details { get; set; }
        [JsonProperty("messages")]
        public List<string> Messages { get; set; } = new List<string>();
        [JsonProperty("errors")]
        public List<LogError> Errors { get; set; } = new List<LogError>();

        #region Insert
        public void Insert()
        {
            DatabaseHelper.InsertLogEntry(this);

            if (Errors != null && Errors.Count > 0)
                DatabaseHelper.InsertLogErrors(this);

            if (Messages != null && Messages.Count > 0)
                DatabaseHelper.InsertLogMessages(this);
        }
        #endregion

        public void Push(string apiUrl)
        {
            if (!string.IsNullOrWhiteSpace(apiUrl))
            {
                HttpHandler.Post(apiUrl, this);
            }
        } 
    }
}