﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Azzysa.Log
{
    // HttpHandler
    public class HttpHandler
    {
        public static void Post(string url, object obj) => PostData(url, obj);

        private static byte[] PostData(string url, object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            var data = PostData(url, System.Text.Encoding.Default.GetBytes(json), "application/json", "application/json");

            return data;
        }

        private static byte[] PostData(string url, byte[] data, string requestContentType = null, string responseContentType = null)
        {
            var request = CreateRequest(RequestMethod.POST, url, requestContentType: requestContentType, responseContentType: responseContentType);

            using (var rs = request.GetRequestStream())
                rs.Write(data, 0, data.Length);

            return GetResponse(request);
        }

        private static HttpWebRequest CreateRequest(RequestMethod method, string url, string requestContentType = null, string responseContentType = null)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method.ToString();

            if (method == RequestMethod.POST)
            {
                if (!string.IsNullOrEmpty(requestContentType))
                    request.ContentType = requestContentType;
            }

            if (!string.IsNullOrEmpty(responseContentType))
                request.Accept = responseContentType;

            return request;
        }

        public enum RequestMethod
        {
            POST,
            GET
        }

        private static byte[] GetResponse(HttpWebRequest request)
        {
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                using (var ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    byte[] array = ms.ToArray();

                    return array;
                }
            }
        }
    }
}
