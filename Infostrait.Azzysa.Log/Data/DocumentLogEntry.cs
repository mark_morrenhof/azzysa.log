﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Log
{
    public class DocumentLogEntry : LogEntry, ILogEntry
    {
        [JsonProperty("fileName")]
        public string FileName { get; set; }
        [JsonProperty("format")]
        public string Format { get; set; }
        [JsonProperty("fileType")]
        public string FileType { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("path")]
        public string Path { get; set; }
        [JsonProperty("fileExists")]
        public bool? FileExists { get; set; }
        [JsonProperty("versionId")]
        public string VersionId { get; set; }
        [JsonProperty("parentVersionExists")]
        public bool? ParentVersionExists { get; set; }
        [JsonProperty("size")]
        public long? Size { get; set; }
        [JsonProperty("relatedObject")]
        public string RelatedObject { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }

        #region Insert
        public new void Insert()
        {
            base.Insert();
            DatabaseHelper.InsertDocumentLogEntry(this);
        }
        #endregion
    }
}