﻿using Infostrait.Azzysa.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiUrl = "http://azzysa.tst.vanderlande.com:5010/api/log";

            var entry = new LogEntry()
            {
                Status = "Info",
                Reason = "Bla die bla",
                Details = "More bladie bla"
            };

            entry.Push(apiUrl);
            //or
            //LogHandler.Log(entry, apiUrl);
        }
    }
}
